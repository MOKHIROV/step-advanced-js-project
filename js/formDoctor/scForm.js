import {info} from "../inputInfo.js";
import Input from "../input.js";
import TextArea from "../textArea.js";
import Select from "../select.js";
export default class SecondForm {
    constructor(doctor){
        this.formCardiologist = document.createElement('form')
        this.formCardiologist.classList.add('second-form')
        this.doctor =doctor;
        this.fullName = new Input(info.fullName, 'modal-input', 'fullName').create();
        this.purpose = new Input(info.purpose, 'modal-input', 'purpose').create();
        this.desc = new TextArea(info.desc, "modal-input", 'desc').create();
        this.pressure = new Input(info.pressure, 'modal-input', '', 'pressure').create();
        this.weightIndex = new Select(info.weightIndex, "form-second-select", 'weightIndex', 'Choose your weight index').create();
        this.illness = new Input(info.illness, "modal-input", 'illness').create();
        this.submit = new Input(info.submit, 'btn-primary-btn', 'btn').create();
    }
    render(modal) {
        this.formCardiologist.append(this.fullName , this.purpose , this.desc , this.pressure , this.weightIndex , this.illness, this.submit )        
        modal.append(this.formCardiologist)
    }
}