import Form from "./form.js";
import Input from "../input.js";
import {info} from "../inputInfo.js";

export default class formDentist extends Form {
	constructor(/* doctor */) {
		super(/* doctor */)
		this.lastDateVisit = new Input(info.lastDateVisit, "modal-input", 'lastDateVisit').create();
		this.ladle = document.createElement('lable')
		this.ladle.innerText = 'дата последнего посещения'
		this.ladle.classList.add('lable-input')
	}
	render(modal) {
        super.render(modal);
        this.lastDateVisit.setAttribute('data-placeholder', 'Last Visit Date');

        this.self.insertBefore(this.lastDateVisit, this.submit);
        this.self.insertBefore(this.ladle,this.lastDateVisit);
        //this.self.insertBefore(this.ladle, parent.firstChild);
        
    }
}
