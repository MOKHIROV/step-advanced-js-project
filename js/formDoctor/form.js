import {info} from "../inputInfo.js";
import Input from "../input.js";
import TextArea from "../textArea.js";
import Select from "../select.js";
export default class Form {
    constructor(doctor) {
        this.self = document.createElement("form");
        this.self.classList.add('form');
        this.doctor = doctor;
        this.fullName = new Input(info.fullName, "modal-input", 'fullName').create();
        this.purpose = new Input(info.purpose, "modal-input", 'purpose').create();
        this.desc = new TextArea(info.desc, "modal-input", 'desc').create();
        this.priority = new Select(info.priority, 'form-select', '', 'priority').create();
        this.date = new Input(info.visitDate, 'modal-input', 'date').create();
        this.submit = new Input(info.submit, 'modal__btn', 'btn').create();
        this.submit.value = 'Create visit';

    }

    render(modal) {
		this.date.setAttribute('data-placeholder', 'Visit Date');
		this.self.append(this.fullName, this.purpose, this.desc, this.priority, this.date, this.submit);
		
        modal.append(this.self);

    }

    renderEdit(modal, id) {
        this.self.classList.remove('form-doctor');
        this.self.classList.add('form-doctor-edit');
        this.self.append(this.fullName, this.purpose, this.desc, this.priority, this.date, this.submit);
        modal.append(this.self);
    }

}