import {info} from "../inputInfo.js";
import Input from "../input.js";
import TextArea from "../textArea.js";
import Select from "../select.js";

export default class FormTherapist{
    constructor(doctor){
        this.therapistForm = document.createElement('form');
        this.therapistForm.classList.add('second-form');
        this.doctor = doctor;
        this.fullName = new Input(info.fullName, 'modal-input', 'fullName').create();
        this.purpose = new Input(info.purpose, 'modal-input', 'purpose').create();
        this.desc = new TextArea(info.desc, "modal-input", 'desc').create();
        this.age = new Input(info.age, 'modal-input', 'age').create();
    }
    render(modal){
        this.therapistForm.append(this.fullName , this.purpose , this.desc, this.age);
        modal.append(this.therapistForm)
    }
}