class Map{
   async signIn(email, password) {
      const response = await  fetch("https://ajax.test-danit.com/api/v2/cards/login", {
            method: 'POST',
            headers: {
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({ email , password })
          })
            const token = await  response.text();
     sessionStorage.setItem('token', token); 
     return response
    }
    getToken(){
      sessionStorage.getItem(console.log('token'))
        return sessionStorage.getItem('token')
    }
    async postCard(body) {
      const response = await fetch('https://ajax.test-danit.com/api/v2/cards', {
          method: 'POST',
          headers: {
              'Content-Type': 'application/json',
              'Authorization': `Bearer 142e1600-8bca-4a09-8573-1ea98be0d86e`
          },
          body: JSON.stringify(body)
      })
      const newCard = await response.json()
    }
    async deleteCard(id){
      const response = await fetch(`https://ajax.test-danit.com/api/v2/cards/${id}` , {
        method: 'DELETE',
        headers: {
          'Authorization': `Bearer 142e1600-8bca-4a09-8573-1ea98be0d86e`
        }
      })
    }
    async putCard(body , id){
      const response = await fetch(`https://ajax.test-danit.com/api/v2/cards/${id}` , {
        method: 'PUT',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': `Bearer 142e1600-8bca-4a09-8573-1ea98be0d86e`
        },
        body : JSON.stringify({body})
      })
      .then(response => response.json())
      .then(response => console.log(response))
    }
}
export default new Map();