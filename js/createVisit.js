import Input from './input.js';
import {info} from './inputInfo.js';
import submitEnterForm from './index.js';
import select from './select.js';
import form from './formDoctor/form.js';
import formDentist from './formDoctor/formDentist.js';
import secondForm from './formDoctor/scForm.js';
import therapistForm from './formDoctor/formTherapist.js'
class Modal{
    constructor() {
        this.wrapper = document.querySelector('#root')
        this.modal = document.createElement('div')
        this.modalDialog = document.createElement('div')
        this.modalContent = document.createElement('div')
        this.modalTitle = document.createElement('p')
        this.form = document.createElement('form')
        this.btnConteiner = document.createElement('div')
        this.btnSubmit = document.createElement('button')
        this.btnCancel = document.createElement('button')
        this.modalDialog.classList.add('modal-dialog')
		this.modalContent.classList.add('modal-content')
		this.modal.classList.add('modal')
		this.modal.style.opacity = '1'
        this.btnCancel.classList.add('modal__btn')
        this.btnSubmit.classList.add('modal__btn')
        this.modalTitle.classList.add('modal__title')
        this.form.classList.add('form')
        this.btnConteiner.append(this.btnSubmit, this.btnCancel)
        


	}	
    renderEnterModal() {
        const inputEmail = new Input(info.email,'modal-input','email').create();
        const inputPassword = new Input(info.password,'modal-input','password').create();

		this.modalDialog.classList.add('modal-dialog')
		this.modalContent.classList.add('modal-content')
		this.modal.classList.add('modal')
		this.modal.style.opacity = '1'
        inputEmail.name = 'email'

        this.modalTitle.innerText = 'Введите Ваши данные'
        this.btnSubmit.innerText = 'Submit'
        this.btnCancel.innerText = 'Cancel'
        
        inputEmail.name = 'email'
        inputPassword.name = 'password'
        this.btnCancel.classList.add('close')
        this.btnConteiner.classList.add('modal__btn-conteiner')
        
		this.wrapper.append(this.modal)
		this.modal.append(this.modalDialog)
		this.modalDialog.append(this.modalContent)
		this.modalContent.append(this.modalTitle,this.form)
		this.form.append(inputEmail,inputPassword,this.btnConteiner)
        this.form.addEventListener('submit',  this.handleSubmitEnterForm.bind(this))
        this.btnCancel.addEventListener('click', () => {
            this.closeModal()
            inputEmail.remove()
            inputPassword.remove()


        })
        window.addEventListener('click', (event) => {
            if (event.target === this.modal) {
                this.closeModal()
                inputEmail.remove()
                inputPassword.remove()
        }
        })
    }
    closeModal() {
        this.modal.remove()
        document.querySelector('.header-btn').removeAttribute('disabled')
        document.querySelector('.header-second-btn').removeAttribute('disabled')
        

        
    }	
    handleSubmitEnterForm(event) {
        event.preventDefault();
        const inputs = this.form.querySelectorAll('input')
        const formData = {};
        inputs.forEach((item) => {
            formData[item.name] = item.value
            item.remove()
        })
        this.btnConteiner.remove()
        console.log(formData);
        submitEnterForm(formData)

        
    }
    
    cart(event) {
        event.preventDefault()  
        const inputs = this.form.querySelectorAll('input')
        const selects = this.form.querySelectorAll('select')
        const textArea = this.form.querySelectorAll('text-area')
        const formData = {};
        inputs.forEach((item) => {
            formData[item.name] = item.value
        })
        selects.forEach((item) => {
            formData[item.name] = item.value
        })
        textArea.forEach((item) => {
            formData[item.name] = item.value
        })
    }
    renderCreateCardModal() {
        this.modalTitle.innerText = 'Выбор врача'
		const selectDoctor = new select(info.docSelect, 'form-select', 'value', 'attr').create()
        this.wrapper.append(this.modal)
		this.modal.append(this.modalDialog)
		this.modalDialog.append(this.modalContent)
        this.modalContent.append(this.modalTitle, this.form)
        this.form.append(selectDoctor)

        this.btnCancel.addEventListener('click', () => {
            this.closeModal()
            selectDoctor.remove()
            

        });
        window.addEventListener('click', (event) => {
            if (event.target === this.modal) {
                const a = document.querySelectorAll('.form')
            
                selectDoctor.remove()

                this.closeModal()
        }
        })
        if (this.form.querySelector('form')) {
            this.form.querySelector('form').remove()
        }
        
        selectDoctor.addEventListener('change', () => {
            this.chooseDoctor(selectDoctor.value)
            
        })
        

    }
    
	chooseDoctor(doctor) {
        const select = this.form.querySelector('select')
        if (this.form.querySelector('form')) {
            this.form.querySelector('form').remove()
        }
        console.log(doctor);
        switch (doctor) {
            case "Cardiologist":
                new secondForm().render(this.form);
                break;
            case "Dentist":
                //if (this.form.querySelector('form')) {
                //    this.form.querySelector('form').remove()
                //}
                new formDentist().render(this.form);
                break;
            case "Therapist":
                new therapistForm().render(this.form);
            //    modal.innerHTML = "";
            //    therapist.render(modal);
            //    break;
        }
	}

}


const modal = new Modal()
export default modal;