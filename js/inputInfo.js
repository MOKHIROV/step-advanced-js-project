export const info = {
    email: {
        type: "email",
        placeholder: "Email",
        isRequired: true
    },

    password: {
        type: "password",
        placeholder: "Password",
        isRequired: true
    },

    cardiologist: {
        type: "text",
        placeholder: "cardiologist",
        isRequired: true
    },

    dentist: {
        type: "text",
        placeholder: "Dentist",
        isRequired: true
    },

    therapist: {
        type: "text",
        placeholder: "Therapist",
        isRequired: true
    },

    fullName: {
        type: "text",
        placeholder: "Full name",
        isRequired: true
    },
    age: {
        type: "text",
        placeholder: "Age",
        isRequired: true
    },
    purpose: {
        type: "text",
        placeholder: "Visit purpose",
        isRequired: true
    },
    desc: {
        placeholder: "Visit description",
        isRequired: true
    },
    pressure: {
        type: "text",
        placeholder: "Pressure",
        isRequired: true
    },
    weightIndex: {
        type: "text",
        placeholder: "Body mass index",
        isRequired: true
    },
    illness: {
        placeholder: "Heart diseases in past",
        isRequired: true
    },
    lastDateVisit: {
        type: "date",
        placeholder: "Last visit date",
        isRequired: true
    },

    visitDate: {
        type: "date",
        placeholder: "Visit date",
        isRequired: true
    },

    submit: {
        type: "submit"
    },

    priority: [
        "Choose urgency",
        "Low",
        "Common",
        "High"
    ],

    docSelect: [
        "Выберите врача",
        "Cardiologist",
        "Dentist",
        "Therapist",
    ],

    cardEdit: [
        "Edit options",
        "Edit",
        "Delete",
    ],
    status: [
        "Choose status",
        "Open",
        "Closed"
    ],
    weightIndex:[
        'Choose your weight index',
        '<16',
        '16 - 18,5',
        '18,5 - 25',
        '25 - 30',
        '30 -35',
        '35 - 40',
        '>40'
    ]
};
