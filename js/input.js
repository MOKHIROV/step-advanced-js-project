export default class Input {
    constructor({type, placeholder, isRequired}, className, attr) {
        this.type = type;
        this.placeholder = placeholder;
        this.isRequired = isRequired;
        this.className = className;
        this.attr = attr;
        this.self = document.createElement("input");
        //this.ladle = document.createElement('lable')
		//this.ladle.innerText = 'дата последнего посещения'
		//this.ladle.classList.add('lable-input')
        
    };

    create() {
        this.self.type = this.type;
        if (this.isUsed(this.placeholder)) {
            this.self.placeholder = this.placeholder;
            this.self.required = this.isRequired;
        }
        this.self.classList.add(this.className);
        this.self.setAttribute('name', this.attr);
        //this.self.insertBefore(this.ladle, parent.firstChild);
        return this.self;

};
    isUsed(prop) {
        return !(prop === null || prop === undefined || prop === "");
    };
};